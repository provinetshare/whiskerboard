Welcome to Wiki
================

This wiki will document the REST-API for Whiskerboard.

Documentation
-------------
- [Services](Services.md)
- [Categories](Categories.md)
- [Events](Events.md)
- [Statuses](Statuses.md)
- [Endpoints and Schemas](Endpoints_and_Schemas.md)


Author
------
 - [Sijis Aviles](https://github.com/sijis)